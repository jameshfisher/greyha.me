module Handler.Blog where

import Import

getBlogR :: Handler RepHtml
getBlogR = defaultLayout $ do
  setTitle "Blog"
  $(widgetFile "blog")

getBlogCssCompressionR :: Handler RepHtml
getBlogCssCompressionR = defaultLayout $ do
  setTitle "CSS compression"
  $(widgetFile "css_compression")
